import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { CacheService } from "ionic-cache";
import { AccountService } from "../../services/account/account.service";
import { PhotoProcessorService } from '../../services/photo/photo-processor.service';
import { Spinner } from "../../services/loader/loader";
import { UserStoreService } from "../../services/auth/user-store.service";
import { extractError } from "../../services/utilities";
import { ErrorHandler } from "../../services/logger.service";
import { ActionBarService } from '../../services/action-bar.service';
import { CustomValidators } from "../../validators/validators";
import { dataURItoBlob } from "../../services/utilities";
import { ENV } from "@environment";

@IonicPage({
  name: "edit-profile",
  defaultHistory: ["profile"]
})
@Component({
  selector: "page-edit-profile",
  templateUrl: "edit-profile.html"
})
export class EditProfilePage {
  userForm: FormGroup;
  account: any = {};
  formSubmitted: boolean = false;
  error: string;
  imgUploadError: string;
  pictureChanged: boolean = false;
  pageLoaded = false;
  pageLoadError = '';
  @ViewChild('filePicker') _filePicker;

  imgUploadActionBar: any = {
    title: "Change your profile picture"
  };

  get firstName() { return this.userForm.get('firstName'); }
  get secondName() { return this.userForm.get('secondName'); }
  get userName() { return this.userForm.get('userName'); }
  get email() { return this.userForm.get('email'); }
  get description() { return this.userForm.get('description'); }
  get password() { return this.userForm.get('password'); }
  get location() { return this.userForm.get('location'); }

  constructor(
    private _navCtrl: NavController,
    private _accountService: AccountService,
    private _formBuilder: FormBuilder,
    private _photoService: PhotoProcessorService,
    private _spinner: Spinner,
    private _userStore: UserStoreService,
    private _cacheService: CacheService,
    private _customValidators: CustomValidators,
    private _actionBarSvc: ActionBarService,
    private _errorHandler: ErrorHandler
  ) { }

  async ionViewDidLoad() {
    try {
      this.account = await this._cacheService.getItem(ENV.CACHE.EDIT_PROFILE.KEY).catch(async () => {
        let data = await this._accountService.getData();
        this._cacheService.saveItem(ENV.CACHE.EDIT_PROFILE.KEY, data, ENV.CACHE.EDIT_PROFILE.KEY, ENV.CACHE.EDIT_PROFILE.TTL);
        return data;
      });

      this.userForm = this.buildForm(this.account);
      this.pageLoaded = true;
    } catch(err) {
      await this._errorHandler.handle(err);
      await this._navCtrl.pop();
    }
  }

  buildForm(account) {
    return this._formBuilder.group(
      {
        userName: [account.userName,
          [
            Validators.required,
            this._customValidators.checkUserName(),
            this._customValidators.allowableLength(450)
          ],
          [
            this._customValidators.nameExist(false, account.userName)
          ]
        ],
        firstName: [account.firstName,
          [
            Validators.required,
            this._customValidators.allowableLength(450)
          ]
        ],
        secondName: [account.secondName,
          [
            this._customValidators.allowableLength(450)
          ]
        ],
        email: [account.email,
          [
            Validators.required,
            Validators.email,
            this._customValidators.allowableLength(450)
          ],
          [
            this._customValidators.emailExist(false, account.email)
          ]
        ],
        location: [
          "",
          [
            this._customValidators.allowableLength(450)
          ]
        ],
        description: [account.description,
          [
            this._customValidators.allowableLength(450)
          ]
        ],
        gender: [account.gender],
        password: [
          "",
          [
          Validators.pattern(/^\S{6,}$/),
            this._customValidators.allowableLength(450)
          ]
        ],
        repeatPassword: [""],
        profilePictureOrig: [account.profilePictureOrig],
        profilePictureMedium: [account.profilePictureMedium]
      },
      { validator: this._customValidators.matchingPasswords("password", "repeatPassword") }
    );
  }

  getProfolePicturePathFormatted(img) {
    if (img) {
      return img.replace(/\\/g, '/');
    } else {
      return "assets/imgs/empty-avatar.png";
    }
  }

  async onChoosePictureFile(file, name) {
    if (!file) {
      return
    }

    let formData: FormData = new FormData();
    formData.append("file", file, name || file.name);

    try {
      let pictures = await this._spinner.appearWhile(() => this._accountService.uploadPicture(formData));

      this.userForm.patchValue( {
        profilePictureOrig: pictures[0],
        profilePictureMedium: pictures[1]
      });

      this.pictureChanged = true
      this.imgUploadError = "";
    } catch (err) {
      this.imgUploadError = extractError(err);
    }
  }

  showChoosePictureWindow(e) {
    e.stopPropagation();

    (this._filePicker.nativeElement as HTMLInputElement).value = "";
    this._actionBarSvc.open({
      ...this.imgUploadActionBar,
      buttons: [
        {
          icon: "helpful-camera-icon-outline",
          text: "Take Photo",
          action: () => {
            this._photoService.getPhotoForRecognition()
              .then((photo) => {
                let file = dataURItoBlob(this._photoService.getJpegDataUrl(photo));
                this.onChoosePictureFile(file, "profilePicture.jpeg");
              })
              .catch((err) => {
                console.warn(err);
              })
          },
        },
        {
          icon: "helpful-photo-frame-icon",
          text: "Choose from library",
          action: () => (this._filePicker.nativeElement as HTMLInputElement).click()
        }
      ]
    });
  }

  async save() {
    this.formSubmitted = true;

    if (!this.userForm.valid) {
      return
    }
    try {
      await this._spinner.appearWhile(async () => {
        await this._accountService.update(this.userForm.value);
        if (this.userForm.value.password) {
          await this._accountService.setPassword(this.userForm.value.password);
        }
      });
      this._navCtrl.pop();
    } catch (err) {
      this.error = extractError(err, true);
      if(!this.error) {
        this._errorHandler.handle(err);
      }
    }
  }

  navigate(page) {
    this._navCtrl.push(page);
  }

  logout() {
    this._userStore.logout();
  }
}
